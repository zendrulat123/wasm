module gitlab.com/zendrulat123/wasm

go 1.16

require (
	github.com/mitchellh/go-homedir v1.1.0
	github.com/slayer/autorestart v0.0.0-20170706172704-7bc8d250279b // indirect
	github.com/spf13/cobra v1.1.3
	github.com/spf13/viper v1.7.1
)
