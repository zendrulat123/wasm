package cmd

import (
	"fmt"
	"os"
	"strconv"
	"time"

	"github.com/spf13/cobra"
	ut "gitlab.com/zendrulat123/wasm/cmd/ut"
)

// watchCmd represents the watch command
var watchCmd = &cobra.Command{
	Use:     "watch",
	Short:   "A brief description of your command",
	Long:    ``,
	Example: `go run *.go watch -p /proj/static/wasm/ -t 3000`,
	Run: func(cmd *cobra.Command, args []string) {
		path, _ := cmd.Flags().GetString("path")
		times, _ := cmd.Flags().GetString("time")
		curr_wd, err := os.Getwd()
		if err != nil {
			fmt.Println(err)
			os.Exit(1)
		}
		i, err := strconv.ParseFloat(times, 64)
		if err != nil {
			fmt.Println(err)

		}
		ticker := time.NewTicker(time.Second * time.Duration(i))

		for t := range ticker.C {
			fmt.Println("Tick at", t)
			ut.Watch(curr_wd + path)
		}

		defer ticker.Stop()
		fmt.Println("Ticker stopped")

	},
}

func init() {
	rootCmd.AddCommand(watchCmd)
	watchCmd.Flags().StringP("path", "p", "", "Set your path")
	watchCmd.Flags().StringP("time", "t", "", "Set your time")
}
func doEvery(d time.Duration, f func(time.Time)) {
	for x := range time.Tick(d) {
		f(x)
	}
}
